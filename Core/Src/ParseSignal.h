/*
 * ParseSignal.h
 *
 *  Created on: 11 нояб. 2020 г.
 *      Author: Oleg
 */

#ifndef SRC_PARSESIGNAL_H_
#define SRC_PARSESIGNAL_H_
#include "main.h"
#include <list>
//#include <deque>
#include <array>
#include <vector>
#include "math.h"
#include <common.h>

#define MAX_samples_from_ADC  8 // количество выборок должно быть покрывать минимум 1 период
#define MAX_saved_samples  MAX_samples_from_ADC * 32
#define ZERO_LEVEL  2047//2047 половина уровня от всего сигнала


class ParseSignal {
public:
	ParseSignal();
	void Parse_halfperiod(void);
	void InitADC_with_DMA(void);

	Fifo_cl<int16_t, MAX_saved_samples>  ADC_data_list;
	uint16_t ADC_Data_arr[MAX_samples_from_ADC] = {0};

	uint8_t ADC_data_ready_1 = 0;
	uint8_t ADC_data_ready_2 = 0;
	uint8_t Parse_data_ready = 0;
//	uint16_t * ADC_Data_ptr = 0;

private:
	void Find_max_min_Ampl(void);
	int16_t IIR_Filter(int16_t);

	Fifo_cl<uint16_t, 20>  Integral_data_list;
	Fifo_cl<uint16_t, 128>  Received_data_list;

	uint8_t Package_ready = 0;
	uint32_t integral = 0;
	uint16_t MINIMAL_LEVEL_C = 4095/24;

	int16_t Previos_signal = 0;
	int16_t Filtered_signal = 0;
    const uint32_t F_sample = 143000;
    const uint32_t F_cutoff = 20000;
    const uint32_t fcutHigh = 5000;
    const uint32_t fcutLow  = 15000; // это правильно что fcutLow > fcutHigh, тк фильтр полосопропускающий
 	const int32_t  K_filterLow = 128 * 1 / (1.0 + F_sample/(2 * 3.14 * fcutLow));
    const int32_t  K_filterHigh = 128 * 1 / (1.0 + (2 * 3.14 * fcutHigh)/F_sample);


};

#endif /* SRC_PARSESIGNAL_H_ */
