/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; Copyright (c) 2019 STMicroelectronics.
  * All rights reserved.</center></h2>
  *
  * This software component is licensed by ST under BSD 3-Clause license,
  * the "License"; You may not use this file except in compliance with the
  * License. You may obtain a copy of the License at:
  *                        opensource.org/licenses/BSD-3-Clause
  *
  ******************************************************************************
  */
/* USER CODE END Header */



/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "DAC.h"
#include "ParseSignal.h"
#include <array>
#include <vector>
/* USER CODE BEGIN */
//#define    DWT_CYCCNT    *(volatile unsigned long *)0xE0001004
//#define    DWT_CONTROL   *(volatile unsigned long *)0xE0001000
//#define    SCB_DEMCR     *(volatile unsigned long *)0xE000EDFC
//
//uint32_t count_tic = 0;

/* USER CODE END */
void cube_init(void);
DAC dac;
ParseSignal parseSignal;
uint8_t data[3] = {0x88,0x88,0x88};
std::vector <uint8_t> StartData = {0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f,0x0f};


int main(void)
{
	//init.INIT_ALL();
	//dac.a = dac.a + 5;
	//int cnt_conversition = 0;
	cube_init();
	parseSignal.InitADC_with_DMA();
	dac.InitPWM_with_DMA();
//	SCB_DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;// разрешаем использовать DWT
//	DWT_CYCCNT = 0;// обнуляем значение
//	DWT_CONTROL|= DWT_CTRL_CYCCNTENA_Msk; // включаем счётчик
//	код
//	count_tic = DWT_CYCCNT;//смотрим сколько натикало

	while (1)
	{
	  dac.sendPacket(StartData);
	  while (dac.SendingPackageState){

		  if (parseSignal.ADC_data_ready_1){
			  //parseSignal.Parse_halfperiod();
			  parseSignal.ADC_data_ready_1 = false;
			  //parseSignal.Parse_data_ready = false;
		  }else if(parseSignal.ADC_data_ready_2){   // && parseSignal.Parse_data_ready
			  LL_ADC_REG_StartConversionSWStart(ADC1); //начало непрерывных прерываний
			  //parseSignal.Parse_halfperiod();
			  parseSignal.ADC_data_ready_2 = false;
			  parseSignal.Parse_data_ready = false;
		  }
		  if (parseSignal.ADC_data_list.size() > 100)
			  parseSignal.Parse_halfperiod();
//		  if (parseSignal.Parse_data_ready){
//			  parseSignal.Parse_data_ready = false;
//				if (!LL_DMA_IsEnabledChannel(DMA1, LL_DMA_CHANNEL_1))
//					LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);
//		  }

	  }


	  //for(int i = 0; i < 10; i++);

	  //dac.sendPacket(data, 3);
	  //while (dac.SendingPackageState);
	  //LL_mDelay(0);
	}
	/* USER CODE END 3 */
}


