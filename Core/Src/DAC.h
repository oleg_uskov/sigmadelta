/*
 * DAC.h
 *
 *  Created on: 10 дек. 2019 г.
 *      Author: uskov.o
 */

#ifndef SRC_DAC_H_
#define SRC_DAC_H_
#include "main.h"
#include "math.h"
#include <common.h>
#include <vector>

#define true 1
#define false 0


#define Number_of_point 10



class DAC {
public:
	DAC();
	void SetNewLevel(uint16_t level, uint8_t positivePeriod) ;
	void InitPWM_with_DMA();
private:
	//void Calculate_Transtining_data_buf(const std::vector<uint8_t> & dataToSend_ptr, const Fifo_cl<uint8_t, 128> & buf);

public:
	Fifo_cl<uint8_t, 128> transm_data_buf;
	uint16_t DAC_Data_arr[32] = {0};
	//uint16_t Base_sin_Array[Number_of_point]; //Array of cosinus data
	const uint8_t Ampl_base = 2; //511
	const uint8_t offset_level = 2;
	uint8_t Ampl = 0;
	//uint16_t Ratio_OfModulation = 1;
	//const int minimalFrequancy = (500000 / Number_of_point)/2;
	//int delta = 1; // set frequancy
	uint8_t bit_in_symbol = 4; // may be 1, 2, 4, 8
	uint8_t positivePeriod = 0;
	uint8_t SendingPackageState = 1;
	uint8_t* dataToSend_ptr;
//	uint8_t mask = 0xFF;
//	int numberOfTransferedByte = 0;
//	int PacketLenght = 0;
	//const uint16_t DAC_ZeroVoltage = 100; //2047



	void GenerateSignal()
	{

		if (transm_data_buf.size() == 0)
		{
			LL_TIM_CC_DisableChannel(TIM2, LL_TIM_CHANNEL_CH1); //запрещаем ШИМ по таймеру
			LL_TIM_DisableCounter(TIM2);
			LL_TIM_DisableCounter(TIM3);
			positivePeriod = 0;
			SendingPackageState = 0;
		}else{
			if(positivePeriod)
				Ampl = transm_data_buf.pop_front();

			SetNewLevel(Ampl, positivePeriod);
			positivePeriod= !positivePeriod;

		}
	}

	void Calculate_Transtining_data_buf(const std::vector<uint8_t> & dataToSend_ptr, Fifo_cl<uint8_t, 128> & buf)
	{
		uint8_t partOfbyte;
		uint8_t Ampl = 0;
		uint16_t i = 0;
		int mask;
		mask = 0xff >> bit_in_symbol; //создаем новую маску
		for (auto data : dataToSend_ptr){
			partOfbyte = 0;
			while (partOfbyte != (8/bit_in_symbol)) // finish to send byte
			{
				Ampl = data >> (bit_in_symbol *  partOfbyte); // (8 >> (bit_in_symbol-1)) ��� ������� �� ���
				Ampl &= mask; // clean unnecessary bit
				buf.push_back(Ampl + Ampl_base);
				DAC_Data_arr[i++] = Ampl + Ampl_base;
				partOfbyte++;
			}

		}

	}
	/*
	void InitArrayOfBaseSin(int Init_freq,int Init_bit_in_symbol) ///???500k/500 = 1k ������� ��� ������ =1
	{
		HalfSinArrayCreating();
		delta = Init_freq/ minimalFrequancy; ///????
		bit_in_symbol = Init_bit_in_symbol;
		mask = (0xFF >> (8 - Init_bit_in_symbol));
	}
	*/

	void sendPacket(const std::vector<uint8_t> & data_ptr) //Ampl in range 0 to 1523
	{
		SendingPackageState = 1; //indicate about starting of transfer data
		//PacketLenght = Lenght;
		//dataToSend_ptr = data_ptr;
		if (transm_data_buf.size() != 0){
			transm_data_buf.clear();
		}
		Calculate_Transtining_data_buf(data_ptr, transm_data_buf);
		LL_TIM_CC_EnableChannel(TIM1, LL_TIM_CHANNEL_CH1); //разрешаем ШИМ по таймеру
		LL_TIM_EnableCounter(TIM1);
		LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);

		LL_TIM_CC_EnableChannel(TIM2, LL_TIM_CHANNEL_CH1); //разрешаем ШИМ по таймеру
		LL_TIM_EnableCounter(TIM2);

		LL_TIM_EnableIT_UPDATE(TIM3);
		LL_TIM_EnableCounter(TIM3);
	}

};


#endif /* SRC_DAC_H_ */
