/*
 * ParseSignal.cpp
 *
 *  Created on: 11 нояб. 2020 г.
 *      Author: Oleg
 */

#include "ParseSignal.h"
#include "stm32f1xx_it.h"
#include "main.h"

#define    DWT_CYCCNT    *(volatile unsigned long *)0xE0001004
#define    DWT_CONTROL   *(volatile unsigned long *)0xE0001000
#define    SCB_DEMCR     *(volatile unsigned long *)0xE000EDFC


extern ParseSignal parseSignal;

ParseSignal::ParseSignal() {

}

void ParseSignal::InitADC_with_DMA() {
		LL_DMA_ConfigTransfer(DMA1,
							LL_DMA_CHANNEL_1,
							LL_DMA_DIRECTION_PERIPH_TO_MEMORY |
							LL_DMA_MODE_CIRCULAR              | //
							LL_DMA_PERIPH_NOINCREMENT         |
							LL_DMA_MEMORY_INCREMENT           |
							LL_DMA_PDATAALIGN_HALFWORD        |
							LL_DMA_MDATAALIGN_HALFWORD        |
							LL_DMA_PRIORITY_HIGH              );
		LL_DMA_ConfigAddresses(DMA1,
							 LL_DMA_CHANNEL_1,
							 LL_ADC_DMA_GetRegAddr(ADC1, LL_DMA_MODE_CIRCULAR), //
							 (uint32_t)&(parseSignal.ADC_Data_arr),
							 LL_DMA_DIRECTION_PERIPH_TO_MEMORY);
		LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_1, MAX_samples_from_ADC);
		LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_1);
		//LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_1);
		//LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_1);
		LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_1);

		LL_ADC_Enable(ADC1);

//		SCB_DEMCR |= CoreDebug_DEMCR_TRCENA_Msk;// разрешаем использовать DWT
//		DWT_CYCCNT = 0;// обнуляем значение
//		DWT_CONTROL|= DWT_CTRL_CYCCNTENA_Msk; // включаем счётчик

		uint32_t wait_loop_index = 0;
		wait_loop_index = ((LL_ADC_DELAY_ENABLE_CALIB_ADC_CYCLES * 32) >> 1);
		while(wait_loop_index != 0)
		{
			wait_loop_index--;
		}
		LL_ADC_StartCalibration(ADC1);
		while (LL_ADC_IsCalibrationOnGoing(ADC1) != 0) {}

		LL_ADC_REG_StartConversionSWStart(ADC1); //начало непрерывных прерываний


}

void ParseSignal::Parse_halfperiod() { ///первая функция для анализа принятых данных по АЦП
	//uint8_t cnt_sample_MINIMAL_LEVEL = 0;
	uint8_t positive_period = 0;
	static int32_t integral = 0;

	uint16_t current_buf_size =  ADC_data_list.size();
	for (uint16_t i = 0; i < current_buf_size; i++){
		int16_t ADC_data = ADC_data_list.pop_front() - ZERO_LEVEL;
		int16_t Filtered_ADC_data = ADC_data; //IIR_Filter(ADC_data);
//		if (Filtered_ADC_data < 0 + MINIMAL_LEVEL_C and Filtered_ADC_data > 0 - MINIMAL_LEVEL_C){ // фиксируем нулевой уровень
//			cnt_sample_MINIMAL_LEVEL++;
//		} else {
//			cnt_sample_MINIMAL_LEVEL = 0;
//		}
		positive_period =  Filtered_ADC_data > MINIMAL_LEVEL_C ? 1 : 0;

		if (positive_period and  Filtered_ADC_data < MINIMAL_LEVEL_C) { //фиксируем переход через 0 в сигнале (???????)
			Integral_data_list.push_back((uint32_t)integral) ; //
			integral = 0;
			if (Integral_data_list.size() == 20) // значит данных достаточно для начала анализа
				Find_max_min_Ampl();
		} else{
			integral = integral + Filtered_ADC_data;
			//ADC_data_list.pop_front(); //очищаем забранные из массива данные
		}
	}
	//parseSignal.ADC_mVolt_list_ready = 0;
	parseSignal.Parse_data_ready = true;
}

void ParseSignal::Find_max_min_Ampl(){
	uint8_t cnt = 0;
	uint16_t AKmax1, AKmax2, AKmin1, AKmin2;
	uint16_t Amax1, Amax2, Amin1, Amin2;
	if (Received_data_list.size() > 127) //?????????  127  ????????????
		Received_data_list.clear(); //чистмм буфер с данными


	for (int i = 0 ; i < Integral_data_list.size() ; i++){
		uint32_t integral = Integral_data_list.pop_front();
		//Amax-Amin = 16 на стороне отправителя
		//AKmax = Amax*K на стороне получателя
		//AKmin = Amin*K
		//(AKmax - AKmin) = 16*K; K = (AKmax - AKmin)/16
		//Amax = AKmax/K = AKmax*16/(AKmax - AKmin)
		//Amin = AKmin/K = AKmin*16/(AKmax - AKmin)
		if (cnt == 0){
			AKmax1 = integral;
			cnt++;
		}else if (cnt == 1){
			AKmax2 = integral;
			cnt++;
		}else if (cnt == 2){
			AKmin1 = integral;
			cnt++;
		}else if (cnt == 3){ //рассчитываем зннчение
			AKmin2 = integral;
			Amax1 = (AKmax1 << 4)/(AKmax1 - AKmin1);
			Amax2 = (AKmax2 << 4)/(AKmax2 - AKmin2);
			Amin1 = (AKmin1 << 4)/(AKmax1 - AKmin1);
			Amin2 = (AKmin2 << 4)/(AKmax2 - AKmin2);
			Received_data_list.push_back(Amax1);
			Received_data_list.push_back(Amax2);
			Received_data_list.push_back(Amin1);
			Received_data_list.push_back(Amin2);
			cnt = 0;
		}

		Integral_data_list.clear();
	}
}


//
//void ParseSignal::Row_data_calculating() {
//	//parseSignal.ADC_mVolt_list.front();
//	for(uint8_t i=0; i<10; i++)	{
//		uint16_t ADC_mVolt;
//		//ADC_mVolt = __LL_ADC_CALC_DATA_TO_VOLTAGE((uint32_t)2990, (parseSignal.ADC_Data)[i], LL_ADC_RESOLUTION_12B);
//	}
//}
inline int16_t  ParseSignal::IIR_Filter(int16_t signal)
	{
		//int Filtered_Low_band_signal = 0;
		Filtered_signal = (int)(K_filterLow*(signal - Filtered_signal))/128 + Filtered_signal;  // фильтруем текущее значение  БИХ фильтром НЧ
		//Filtered_signal = (int)(K_filterHigh*(Filtered_signal + Filtered_Low_band_signal - Previos_signal) )/128;  // фильтруем текущее значение  БИХ фильтром DЧ
		Previos_signal = signal;
		return Filtered_signal;
	}

void ADC_DMA_HalfTransferComplete_Callback()
{
//	uint16_t * first_half_ptr = &parseSignal.ADC_Data_arr[0];
//	parseSignal.ADC_Data_ptr = first_half_ptr;
//	if(!parseSignal.Parse_data_ready) { // значит данные до сих пор не отфильтрованы
//		//LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_1);
//		Error_Handler();
//	}

	parseSignal.ADC_data_ready_1 = 1;
}

void ADC_DMA_TransferComplete_Callback()
{
	if (parseSignal.ADC_data_list.size() >= MAX_saved_samples)
		parseSignal.ADC_data_list.clear(); //чистмм необработанный мусор(по хорошему должны обработать )

	for(uint8_t i = 0; i < MAX_samples_from_ADC; i++)
	{
		parseSignal.ADC_data_list.push_back(parseSignal.ADC_Data_arr[i]);
	}
//	uint16_t * second_half_ptr = &parseSignal.ADC_Data_arr[MAX_samples_from_ADC/2];
//	parseSignal.ADC_Data_ptr = second_half_ptr;
//	if(!parseSignal.Parse_data_ready) { // значит данные до сих пор не отфильтрованы
//		LL_DMA_DisableChannel(DMA1, LL_DMA_CHANNEL_1);
//		Error_Handler();
//	}
	parseSignal.ADC_data_ready_2 = 1;
	//LL_ADC_REG_StartConversionSWStart(ADC1); //начало непрерывных прерываний
//		int count_tic = 0;
//		count_tic = DWT_CYCCNT;//смотрим сколько натикало

}


