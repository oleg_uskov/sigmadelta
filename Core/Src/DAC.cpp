/*
 * DAC.cpp
 *
 *  Created on: 10 дек. 2019 г.
 *      Author: uskov.o
 */

#include "DAC.h"
#include "stm32f1xx_it.h"

extern DAC dac;

DAC::DAC() {
	//Set the Capture Compare Registers value
	WRITE_REG(TIM2->CCR1, offset_level);
}

inline void DAC::SetNewLevel(uint16_t level, uint8_t positivePeriod) {

	if (positivePeriod)
		CLEAR_BIT(TIM2->CCMR1,TIM_CCMR1_OC1M_0); // прямой выход ШИМ
	else
		SET_BIT(TIM2->CCMR1,TIM_CCMR1_OC1M_0); // инверсный выход ШИМ
	WRITE_REG(TIM2->ARR, level-1 + 2*offset_level);
	//Set the Capture Compare Registers value
	//WRITE_REG(TIM2->CCR1, offset_level);

}

void DAC::InitPWM_with_DMA() {
	LL_DMA_ConfigTransfer(DMA1,
							LL_DMA_CHANNEL_5,
							LL_DMA_DIRECTION_MEMORY_TO_PERIPH |
							LL_DMA_MODE_NORMAL                | //
							LL_DMA_PERIPH_NOINCREMENT         |
							LL_DMA_MEMORY_INCREMENT           |
							LL_DMA_PDATAALIGN_HALFWORD        |
							LL_DMA_MDATAALIGN_HALFWORD        |
							LL_DMA_PRIORITY_MEDIUM            );
		LL_DMA_ConfigAddresses(DMA1,
							 LL_DMA_CHANNEL_5,
							 (uint32_t)&(dac.DAC_Data_arr), //
							 (uint32_t)&(TIM1->ARR),
							 LL_DMA_DIRECTION_MEMORY_TO_PERIPH);
		LL_DMA_SetDataLength(DMA1, LL_DMA_CHANNEL_5, 32);
		LL_DMA_EnableIT_TC(DMA1, LL_DMA_CHANNEL_5);
		//LL_DMA_EnableIT_HT(DMA1, LL_DMA_CHANNEL_5);
		//LL_DMA_EnableIT_TE(DMA1, LL_DMA_CHANNEL_5);
		LL_DMA_EnableChannel(DMA1, LL_DMA_CHANNEL_5);

}


 void TIM3_Callback()
{

	 if(LL_TIM_IsActiveFlag_UPDATE(TIM3))
		 LL_TIM_ClearFlag_UPDATE(TIM3); //сброс флага прерывания по таймеру
	 //dac.GenerateSignal();

	 dac.SetNewLevel(15, dac.positivePeriod);
	 	 dac.positivePeriod= !dac.positivePeriod;

}


