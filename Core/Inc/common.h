/*
 * common.hpp
 *
 *  Created on: 23 нояб. 2020 г.
 *      Author: Oleg
 */

#ifndef INC_COMMON_H_
#define INC_COMMON_H_

#include <array>

template < typename T, size_t max_size >
    class Fifo_cl
    {
    public:
		Fifo_cl(): push_index(0), size_valid_data(0)
		{
		}

    private:
		unsigned push_index;
		unsigned size_valid_data;
		std::array<T, max_size> fifo;

    public:
		void push_back(T new_value)
		{
			if (size_valid_data < max_size) {
				//fifo[push_index++ % max_size] = new_value;
				fifo[push_index] = new_value;
				push_index = (push_index < max_size - 1) ? push_index + 1 : 0;
				size_valid_data++;
			}
		}

		uint8_t size()
		{
			return size_valid_data;
		}

		void clear()
		{
			size_valid_data = 0;
		}


		T pop_front()
		{
			if (size_valid_data == 0)
				return 0;
			if (push_index > size_valid_data)
				return fifo[push_index - size_valid_data--];
			else
				return fifo[push_index + max_size - size_valid_data--];
		}

    };



#endif /* INC_COMMON_H_ */
